package main

import (
	"github.com/soudy/gargoyle/smtp"
	"log"
)

const (
	CONN_HOST = "localhost"
	CONN_PORT = "2525"

	VERSION = "0.1"
)

var (
	domain = "smtp.local"
	server *smtp.Server
)

func main() {
	log.Printf("Gargoyle %s", VERSION)

	server = smtp.NewServer(CONN_HOST, CONN_PORT)
	server.ListenAndServe()
}
