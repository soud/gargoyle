package smtp

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"net"
	"regexp"
	"strings"
	"time"
)

type Server struct {
	Hostname string
	Host     string
	Port     string

	listener net.Listener
}

type Client struct {
	conn   net.Conn
	server *Server
	rb     *bufio.Reader

	helo       string
	from       string
	recipients []string
	data       string
}

func NewServer(host, port string) *Server {
	return &Server{
		Host: host,
		Port: port,
	}
}

var (
	mailFromRE = regexp.MustCompile(`(?i)From:\s*?<(.+)>`)
	rcptRE     = regexp.MustCompile(`(?i)To:\s*?<(.+)>`)
)

func (s *Server) Serve() {
	defer s.Stop()

	for {
		conn, err := s.listener.Accept()
		if err != nil {
			log.Printf("Error accepting: %s", err.Error())
			break
		}

		go s.handleConn(&Client{
			conn:   conn,
			server: s,
			rb:     bufio.NewReader(conn),
		})
	}
}

func (s *Server) ListenAndServe() {
	ln, err := net.Listen("tcp", s.Host+":"+s.Port)
	if err != nil {
		log.Fatalln("Error listening:", err.Error())
	}

	s.listener = ln

	log.Printf("Listening on %s:%s", s.Host, s.Port)

	s.Serve()
}

func (s *Server) handleConn(c *Client) {
	log.Printf("New connection <%s>", c.Addr())

	defer c.conn.Close()

	c.conn.SetReadDeadline(time.Now().Add(5 * time.Minute))
	c.Write("220", fmt.Sprintf("%s SMTP Gargoyle", s.Hostname))

	for {
		line, err := c.rb.ReadSlice('\n')

		if err == nil {
			c.handle(string(line))
		} else {
			if err == io.EOF {
				break
			}

			if neterr, ok := err.(net.Error); ok && neterr.Timeout() {
				c.Write("221", "Idle timout")
				break
			}

			c.Write("221", "Connection error")
			break
		}
	}

	log.Printf("Closing connection <%s>", c.Addr())
}

func (c *Client) handle(msg string) {
	msg = strings.TrimRight(msg, "\r\n")

	if msg == "" {
		c.Write("500", "Please say something")
		return
	}

	slices := strings.Split(msg, " ")
	cmd, arg := strings.ToUpper(slices[0]), strings.Join(slices[1:], " ")

	log.Printf("Received: %s", msg)

	switch cmd {
	case "HELO", "EHLO":
		c.handleHello(cmd, arg)
	case "MAIL":
		c.handleMail(arg)
	case "RCPT":
		c.handleRcpt(arg)
	case "DATA":
		c.Write("250", "data!")
	case "RSET":
		c.reset()
		c.Write("250", "OK")
	case "SEND":
		c.Write("250", "send!")
	case "HELP":
		c.Write("214", "help!")
	case "NOOP":
		c.Write("250", "OK")
	case "QUIT":
		c.Write("221", "Bye bye")
		c.conn.Close()
	default:
		c.Write("500", "Command not recognized")
	}
}

func (c *Client) handleHello(cmd string, arg string) {
	if arg == "" {
		c.Write("501", "Syntax: HELO hostname")
		return
	}

	c.helo = arg
	c.Write("250", "OK")
}

func (c *Client) handleMail(arg string) {
	if c.helo == "" {
		c.Write("503", "Send HELO/EHLO first")
		return
	}

	if arg == "" {
		c.Write("501", "Syntax: MAIL FROM:<address>")
		return
	}

	if c.from != "" {
		c.Write("503", "Error: nested MAIL command")
		return
	}

	match := mailFromRE.FindStringSubmatch(arg)
	if match == nil {
		c.Write("501", "Bad sender address syntax")
		return
	}

	c.from = match[1]

	c.Write("250", "OK")
}

func (c *Client) handleRcpt(arg string) {
	if c.from == "" {
		c.Write("503", "Error: need MAIL command")
		return
	}

	if arg == "" {
		c.Write("501", "Syntax: RCPT TO:<address>")
	}

	match := rcptRE.FindStringSubmatch(arg)
	if match == nil {
		c.Write("501", "Bad recipient address syntax")
		return
	}

	c.recipients = append(c.recipients, match[1])

	c.Write("250", "OK")
}

func (c *Client) Addr() net.Addr {
	return c.conn.RemoteAddr()
}

func (c *Client) Write(code, msg string) {
	c.conn.Write([]byte(code + " " + msg + "\r\n"))
	log.Printf("Sent: %s", code+" "+msg)
}

func (c *Client) reset() {
	c.helo = ""
	c.from = ""
	c.recipients = nil
}

func (s *Server) Stop() {
	log.Printf("Shutting down")
	s.listener.Close()
}
